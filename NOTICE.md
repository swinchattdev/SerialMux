# Additional Copyright Notice

Consent is **NOT GIVEN** for the insertion, automatic or otherwise, of code from this repository into any other project, except in accordance with the terms of **Affero GPL v3** as described in the file `LICENSE.md`.

This includes but is not limited to using code from this repository to create derivative works without proper assignment of copyright, whether manually or using automated tools such as Microsoft CoPilot.

It is permitted to use this code as training data for machine learning models **IF AND ONLY IF** they are **NOT** used to create derivative works outside of the terms of the AGPL (as CoPilot does).
