# SerialMux

Serial mux -- multiplex serial I/O

## Usage

```bash
serialmux <serial> [more serials]
````

### Options

Short Option | Long Option | Description
-------------|-------------|------------
`-h`         | `--help`    | Show this help and quit

### Examples

```bash
serialmux COM1 COM3                 # Multiplex COM1 and COM3 (Windows)
serialmux /dev/ttyS0 /dev/ttyS1     # Multiplex /dev/ttyS0 and /dev/ttyS1
```
